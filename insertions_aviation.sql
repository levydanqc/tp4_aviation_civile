
--
-- Données pour BD aviation_civile - 07B_TP4_H2021
--

insert into compagnie values('Air Canada','CA','international','Toronto',36000,166,225,1937);
insert into compagnie values('Air Transat','CA','international','Montréal',5100,38,67,1987);
insert into compagnie values('Aer Lingus','IE','international','Dublin',12000,54,70,1936);
insert into compagnie values('Ryan Air','IE','international','Dublin',17500,370,225,1985);
insert into compagnie values('Aeroflot','RU','international','Moscou',30400,253,146,1923);

insert into avions values(null,'Airbus','330-200','FR','bi-couloir',2,1998,null);
insert into avions values(null,'Boeing','787-8','US','bi-couloir',2,2012,null);
insert into avions values(null,'Airbus','320-200','DE','mono-couloir',2,1989,null);
insert into avions values(null,'Boeing','777-300','US','bi-couloir',2,1994,null);

insert into moteurs values(null,'General Electric','CF6',310,4000,1971,null);
insert into moteurs values(null,'Rolls Royce','Trent',431,6000,1987,null);
insert into moteurs values(null,'CFM','56',118,2270,1979,null);
insert into moteurs values(null,'General Electric','GE90',512,8760,1993,null);

insert into loueur values('AerCap','IE',1200,'wet lease');
insert into loueur values('DAE','AE',370,'wet lease');