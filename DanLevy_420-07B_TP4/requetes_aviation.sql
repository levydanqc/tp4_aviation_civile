-- ----------------------------------------------------------------------------
-- PARTIE 2: Insertion de données 
-- ----------------------------------------------------------------------------

-- Ajout de l'avion en service Boeing 787-8
insert into avions_service values (
    "C-GHPQ",
    "Air Canada",
    (select id_avion from avions where constructeur="Boeing" and modele="787-8"),
    (select id_moteur from moteurs where constructeur="Rolls Royce" and modele="Trent"),
    255,
    "3 classes",
    "28 LD3",
    2014,
    2014,
    null,
    15000,
    "actif"
);

insert into avions_service values (
    "C-GGTS",
    "Air Transat",
    (select id_avion from avions where constructeur="Airbus" and modele="330-200"),
    (select id_moteur from moteurs where constructeur="General Electric" and modele="CF6"),
    345,
    "2 classes",
    "26 LD3",
    1999,
    2014,
    "AerCap",
    55000,
    "parqué"
);

insert into avions_service values (
    "C-FKPT",
    "Air Canada",
    (select id_avion from avions where constructeur="Airbus" and modele="330-200"),
    (select id_moteur from moteurs where constructeur="CFM" and modele="56"),
    146,
    "2 classes",
    "7 LD3",
    1992,
    1992,
    null,
    68000,
    "actif"
);

insert into pilotes values (
    null,
    "Solo",
    "Yann",
    "1970-10-26",
    15000,
    (select id_avion from avions where constructeur="Boeing" and modele="787-8"),
    "pilote-1",
    "Air Canada"
);

insert into pilotes values (
    null,
    "Bart",
    "Simpson",
    "1981-02-23",
    6500,
    (select id_avion from avions where constructeur="Airbus" and modele="330-200"),
    "co-pilote-1",
    "Air Transat"
);


-- ----------------------------------------------------------------------------
-- PARTIE 4: Requête de sélection 
-- ----------------------------------------------------------------------------
