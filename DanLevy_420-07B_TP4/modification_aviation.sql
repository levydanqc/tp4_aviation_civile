-- ----------------------------------------------------------------------------
-- PARTIE 1: DDL 
-- ----------------------------------------------------------------------------

-- Suppression de la colonne puissance de la table moteurs.
alter table moteurs drop column puissance;

-- Ajout du champ poussee à la table moteurs.
alter table moteurs add poussee int(4) after modele;

-- Modification du champs loueur afin de permettre la valeur null.
alter table avions_service change loueur loueur varchar(50) null;

-- Ajout du champ status à la table avions_service.
alter table avions_service add status enum("actif", "parqué", "maintenance") not null;

-- Ajout du champ nombre_avions à la table compagnie.
alter table compagnie add nombre_avions int(4) after nombre_employes;


-- ----------------------------------------------------------------------------
-- PARTIE 3: Modification de contraintes 
-- ----------------------------------------------------------------------------

-- avions_service_ibfk_1: null à la suppression et m-à-j à la modification.
-- Suppression préalable de la contrainte pour l'ajout de la nouvelle et
-- ajout de la possibilité de la valeur null au champ compagnie de avions_service
alter table avions_service drop foreign key avions_service_ibfk_1;

alter table avions_service change compagnie compagnie varchar(50) null;

alter table avions_service add constraint avions_service_ibfk_1
foreign key (compagnie) references compagnie (nom) on 
delete set null on update cascade;

--  pilotes_ibfk_2: null à la suppression et m-à-j à la modification.
alter table pilotes drop foreign key pilotes_ibfk_2;

alter table pilotes change compagnie compagnie varchar(50) null;

alter table pilotes add constraint pilotes_ibfk_2
foreign key (compagnie) references compagnie (nom) on 
delete set null on update cascade;

--  avions_service_ibfk_3: restreindre à la suppression et m-à-j à la modification.
alter table avions_service drop foreign key avions_service_ibfk_3;

alter table avions_service add constraint avions_service_ibfk_3
foreign key (avion) references avions (id_avion) on 
DELETE restrict on update cascade;

--  pilotes_ibfk_1: null à la suppression et restreindre à la modification.
alter table pilotes drop foreign key pilotes_ibfk_1;

alter table pilotes change avion avion int(11) null;

alter table pilotes add constraint pilotes_ibfk_1
foreign key (avion) references avions (id_avion) on 
delete set null on update restrict;

--  avions_service_ibfk_4: null à la suppression et restreindre à la modification.
alter table avions_service drop foreign key avions_service_ibfk_4;

alter table avions_service change moteur moteur int(11) null;

alter table avions_service add constraint avions_service_ibfk_4
foreign key (moteur) references moteurs (id_moteur) on 
DELETE set null on update restrict;

--  avions_service_ibfk_2: m-à-j à la suppression et m-à-j à la modification.
alter table avions_service drop foreign key avions_service_ibfk_2;

alter table avions_service change loueur loueur varchar(50) null;

alter table avions_service add constraint avions_service_ibfk_2
foreign key (loueur) references loueur (nom) on 
DELETE cascade on update cascade;
