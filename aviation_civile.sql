-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 18 avr. 2021 à 15:40
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `aviation_civile`
--
CREATE DATABASE IF NOT EXISTS `aviation_civile` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `aviation_civile`;

-- --------------------------------------------------------

--
-- Structure de la table `avions`
--

DROP TABLE IF EXISTS `avions`;
CREATE TABLE IF NOT EXISTS `avions` (
  `id_avion` int(11) NOT NULL AUTO_INCREMENT,
  `constructeur` varchar(30) NOT NULL,
  `modele` varchar(30) NOT NULL,
  `pays` varchar(30) NOT NULL,
  `fuselage` enum('mono-couloir','bi-couloir') NOT NULL,
  `nombre_moteurs` int(1) NOT NULL,
  `annee_lancement` year(4) NOT NULL,
  `annee_arret` year(4) DEFAULT NULL,
  PRIMARY KEY (`id_avion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `avions_service`
--

DROP TABLE IF EXISTS `avions_service`;
CREATE TABLE IF NOT EXISTS `avions_service` (
  `immatriculation` varchar(9) NOT NULL,
  `compagnie` varchar(50) NOT NULL,
  `avion` int(11) NOT NULL,
  `moteur` int(11) NOT NULL,
  `nombre_sieges` int(3) NOT NULL,
  `configuration` enum('1 classe','2 classes','3 classes') NOT NULL,
  `capacite_cargo` varchar(6) NOT NULL,
  `annee_production` year(4) NOT NULL,
  `annee_acquisition` year(4) NOT NULL,
  `loueur` varchar(50) NOT NULL,
  `heures_de_vol` int(7) NOT NULL,
  PRIMARY KEY (`immatriculation`),
  KEY `avion` (`avion`),
  KEY `moteur` (`moteur`),
  KEY `loueur` (`loueur`),
  KEY `compagnie` (`compagnie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `compagnie`
--

DROP TABLE IF EXISTS `compagnie`;
CREATE TABLE IF NOT EXISTS `compagnie` (
  `nom` varchar(50) NOT NULL,
  `pays` varchar(30) NOT NULL,
  `reseau` enum('domestique','international') NOT NULL,
  `base` varchar(30) NOT NULL,
  `nombre_employes` int(6) NOT NULL,
  `nombre_destination` int(3) NOT NULL,
  `annee_creation` year(4) NOT NULL,
  PRIMARY KEY (`nom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `loueur`
--

DROP TABLE IF EXISTS `loueur`;
CREATE TABLE IF NOT EXISTS `loueur` (
  `nom` varchar(50) NOT NULL,
  `pays` varchar(30) NOT NULL,
  `nombre_avions` int(4) NOT NULL,
  `lease_type` enum('wet lease','dry lease') NOT NULL,
  PRIMARY KEY (`nom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `moteurs`
--

DROP TABLE IF EXISTS `moteurs`;
CREATE TABLE IF NOT EXISTS `moteurs` (
  `id_moteur` int(11) NOT NULL AUTO_INCREMENT,
  `constructeur` varchar(30) NOT NULL,
  `modele` varchar(30) NOT NULL,
  `puissance` int(7) NOT NULL,
  `poids` int(6) NOT NULL,
  `annee_lancement` year(4) NOT NULL,
  `annee_arret` year(4) DEFAULT NULL,
  PRIMARY KEY (`id_moteur`),
  KEY `constructeur` (`constructeur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `pilotes`
--

DROP TABLE IF EXISTS `pilotes`;
CREATE TABLE IF NOT EXISTS `pilotes` (
  `id_pilote` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `date_de_naissance` date NOT NULL,
  `heures_de_vol` int(5) NOT NULL,
  `avion` int(11) NOT NULL,
  `grade` enum('pilote-1','pilote-2','co-pilote-1','co-pilote-2') NOT NULL,
  `compagnie` varchar(50) NOT NULL,
  PRIMARY KEY (`id_pilote`),
  KEY `avion` (`avion`),
  KEY `compagnie` (`compagnie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `avions_service`
--
ALTER TABLE `avions_service`
  ADD CONSTRAINT `avions_service_ibfk_1` FOREIGN KEY (`compagnie`) REFERENCES `compagnie` (`nom`),
  ADD CONSTRAINT `avions_service_ibfk_2` FOREIGN KEY (`loueur`) REFERENCES `loueur` (`nom`),
  ADD CONSTRAINT `avions_service_ibfk_3` FOREIGN KEY (`avion`) REFERENCES `avions` (`id_avion`),
  ADD CONSTRAINT `avions_service_ibfk_4` FOREIGN KEY (`moteur`) REFERENCES `moteurs` (`id_moteur`);

--
-- Contraintes pour la table `pilotes`
--
ALTER TABLE `pilotes`
  ADD CONSTRAINT `pilotes_ibfk_1` FOREIGN KEY (`avion`) REFERENCES `avions` (`id_avion`),
  ADD CONSTRAINT `pilotes_ibfk_2` FOREIGN KEY (`compagnie`) REFERENCES `compagnie` (`nom`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
